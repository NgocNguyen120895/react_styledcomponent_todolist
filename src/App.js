import './App.css';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import HomePage from './Pages/HomePage/HomePage';
import LoginPage from './Pages/LoginPage/LoginPage';
import DetailPage from './Pages/HomePage/DetailPage/DetailPage';
import Layout from './Layout/Layout';
import BookingPage from './Pages/BookingPage/BookingPage';
import NotFoundPage from './Pages/NotFound/NotFoundPage';




export default function App() {
  return (
    <div>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<HomePage />} />
          <Route path="/login" element={<LoginPage />} />
          <Route path="/detail/:id" element={<Layout Component={DetailPage} />} />
          <Route path="/booking/:id" element={<Layout Component={BookingPage} />} />
          <Route path="*" element={<Layout Component={NotFoundPage} />} />
        </Routes>
      </BrowserRouter>
    </div>
  )
}

