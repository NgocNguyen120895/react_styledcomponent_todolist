import React from 'react';
import Footer from '../Pages/Components/Footer/Footer';
import Header from '../Pages/Components/Header/Header';

export default function Layout({ Component }) {

    // let com = Component;
    // console.log(Component)
    return (
        <div className='min-h-screen h-full flex flex-col space-y-10'>
            <Header />
            <div className='flex-grow'>
                <Component />
            </div>
            <Footer />
        </div>
    )
}
