import React from 'react'
import { Card } from 'antd';
import { NavLink } from 'react-router-dom';
const { Meta } = Card;

export default function ItemMovie({ data }) {
    return (
        <Card
            hoverable
            style={{
                width: 240,
            }}
            cover={<img className='h-40 object-cover object-top' alt="example" src={data.hinhAnh} />}
        >
            <Meta title={data.tenPhim} description={<NavLink to={`/detail/${data.maPhim}`} className={"px-5 py-2 border-2 border-red-500 rounded"}>Xem ngay</NavLink >} />
        </Card>
    )
}
