import React from 'react'
import UserDropdown from './UserDropdown'
import UserMenu from './UserMenu'

export default function HeaderDesktop() {
    return (
        <div className='h-20 shadow w-full'>
            <div className="container mx-auto h-full flex items-center justify-between">
                <span className='font-medium text-red-500 animate-pulse'>CYBERFLIX</span>
                <div className='items-center justify-center'>
                    <span>Lich Chieu</span>
                    <span>Cum Rap</span>
                    <span>Tin Tuc</span>
                    <span>Ung Dung</span>
                </div>
                <UserMenu />
            </div>
        </div>
    )
}
