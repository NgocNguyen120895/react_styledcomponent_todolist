import React from 'react';
import { DownOutlined } from '@ant-design/icons';
import { Dropdown, Space } from 'antd';


export default function UserDropdown({ user, logoutBtn }) {
    console.log(user)


    const items = [
        {
            key: '1',
            label: <button onClick={logoutBtn}>Logout</button>,
        },
        {
            key: '2',
            label: <span>Cap nhat tai khoan</span>
        },

    ];

    // console.log(user.hoTen)
    return (
        <div>
            <Dropdown
                menu={{
                    items,
                }}
            >
                <a onClick={(e) => e.preventDefault()}>
                    <Space>
                        {user.hoTen}
                        <DownOutlined />
                    </Space>
                </a>
            </Dropdown>
        </div>

    )
}
