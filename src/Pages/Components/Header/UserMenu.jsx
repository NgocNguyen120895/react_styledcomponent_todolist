import React from 'react';
import { useSelector } from "react-redux";
import { NavLink } from 'react-router-dom';
import { localUserService } from '../../services/localService';
import UserDropdown from './UserDropdown';

export default function UserMenu() {

    let userInfo = useSelector((state) => {
        return state.userReducer.userInfo;
    })

    // console.log(userInfo);

    let handleLogout = () => {
        localUserService.remove();
        window.location.reload();
        // window.location.href = "/login";
    }

    let renderContent = () => {

        let buttonCss = "px-1 mx-2 py-1 border-2 border-black rounded";

        if (userInfo) {
            return (
                <>
                    <UserDropdown user={userInfo} logoutBtn={handleLogout}/>
                    {/* <button className={buttonCss} onClick={handleLogout}>Dang Xuat</button> */}
                </>
            )
        } else {
            return (
                <>
                    <NavLink to="/login">
                        <button className={buttonCss}>Dang nhap</button>
                    </NavLink>
                    <button className={buttonCss}>Dang ky</button>
                </>
            )
        }
    }
    return (
        <div>{renderContent()}</div>
    )
}
