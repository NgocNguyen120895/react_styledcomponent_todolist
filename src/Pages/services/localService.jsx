
export const localUserService = {
    set: (userInfo) => {
        let dataJson = JSON.stringify(userInfo);
        localStorage.setItem("USER_INFO", dataJson);
    },

    get: () => {
        let dataJson = localStorage.getItem("USER_INFO");
        return JSON.parse(dataJson);
    },

    remove: () => {
        localStorage.removeItem("USER_INFO");
    },
}