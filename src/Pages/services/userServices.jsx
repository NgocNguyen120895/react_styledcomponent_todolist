import { BASE_URL, headers } from "./config";
import axios from "axios";

export const userSevices = {
    postLogin: (userLoginForm) => {
        return axios({
            url: `${BASE_URL}/api/QuanLyNguoiDung/DangNhap`,
            method: "POST",
            data: userLoginForm,
            headers: headers(),
        })
    }
}