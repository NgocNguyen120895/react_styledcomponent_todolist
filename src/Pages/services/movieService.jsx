import { BASE_URL, https } from "./config"

export const movieService = {

    getMovieList: () => {
        return https.get(`${BASE_URL}/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP00`);
    },

    getMovieByTheater: () => {
        return https.get(`${BASE_URL}/api/QuanLyRap/LayThongTinLichChieuHeThongRap?maNhom=GP01`);
    },

    getMovieDetail: (movieId) => {
        return https.get(`${BASE_URL}/api/QuanLyPhim/LayThongTinPhim?MaPhim=${movieId}`);
    }
} 