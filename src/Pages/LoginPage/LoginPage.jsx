import React from 'react';
import { Button, Form, Input, message } from 'antd';
import { userSevices } from '../services/userServices';
import { localUserService } from '../services/localService';
import { useNavigate } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import Lottie from "lottie-react";
import animate from "../assets/animate.json";
import { setLoginAction, setLoginActionService } from '../../Redux/action/userAction';




export default function LoginPage() {
    let dispatch = useDispatch();
    let navigate = useNavigate();


    // const onFinish = (values) => {
    //     userSevices.postLogin(values)
    //         .then((res) => {
    //             console.log(res);
    //             message.success("Dang nhap thanh cong");

    //             // Lưu localstore
    //             localUserService.set(res.data.content);

    //             // Chuyển hướng User
    //             navigate("/home");

    //             //Redux
    //             dispatch(
    //                 setLoginAction(res.data.content)
    //             );
    //         })
    //         .catch((err) => {
    //             console.log(err);
    //             message.error("That bai")
    //         });
    // };

    const onFinishThunk = (value) => {
        let onSuccess = () => {
            message.success("Dang nhap thanh cong");
            navigate("/");
        }
        dispatch(setLoginActionService(value, onSuccess));
    }

    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };

    return (
        <div className="h-screen w-screen flex bg-orange-500 justify-center items-center">
            <div className="container mx-auto p-5 bg-white rounded flex">
                <div className='w-1/2 h-full'>
                    <Lottie animationData={animate} loop={true}></Lottie>
                </div>
                <div className='w-1/3 flex'>
                    <Form name="basic"
                        labelCol={{
                            span: 8,
                        }}
                        wrapperCol={{
                            span: 24,
                        }}
                        style={{
                            width: "50%%",
                        }}
                        initialValues={{
                            remember: true,
                        }} onFinish={onFinishThunk} onFinishFailed={onFinishFailed} layout='vertical' autoComplete="off">
                        <Form.Item
                            label="Username"
                            name="taiKhoan"
                            rules={[
                                {
                                    required: true,
                                    message: 'Please input your username!',
                                },
                            ]}
                        >
                            <Input />
                        </Form.Item>
                        <Form.Item
                            label="Password"
                            name="matKhau"
                            rules={[
                                {
                                    required: true,
                                    message: 'Please input your password!',
                                },
                            ]}
                        >
                            <Input.Password />
                        </Form.Item>

                        <Form.Item className='flex justify-center'
                            wrapperCol={{ span: 16, }}>
                            <Button className='bg-orange-500 hover:border-none' type="primary" htmlType="submit">
                                Submit
                            </Button>
                        </Form.Item>
                    </Form>
                </div>
            </div>
        </div >
    )


}




