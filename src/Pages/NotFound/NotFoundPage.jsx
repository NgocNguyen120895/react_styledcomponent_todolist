import React from 'react'
import { NavLink } from 'react-router-dom'
import "../NotFound/notFound.css"

export default function NotFoundPage() {
    return (
        <div className='notFound'>
            {/* <!-- Code and Design By Bradley Budach --> */}

            <h1>Error 404</h1>
            <center>
                <img src="/not-found.svg" alt="" />
                <p>Were Sorry the page you were looking for was stomped on by a dinosaur. Make sure you typed in the right address, or try again later.</p>
                <NavLink to="/">
                    <button>Back To Untrampled Ground</button>
                </NavLink>
            </center>
        </div>
    )
}

