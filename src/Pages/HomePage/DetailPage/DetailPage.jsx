import React, { useEffect, useState } from 'react';
import { NavLink, useParams } from 'react-router-dom';
import { movieService } from '../../services/movieService';
import { Progress } from 'antd';


export default function DetailPage() {
    let { id } = useParams();
    let [movie, setMovie] = useState({});

    useEffect(() => {
        let fetchDetail = async () => {
            try {
                let result = await movieService.getMovieDetail(id);
                // console.log("result", result);
                setMovie(result.data.content);
            } catch (e) {
                console.log(e)
            }
        };
        fetchDetail();
    })

    return (
        <div className='h-40 container'>
            <img src={movie.hinhAnh} className="w-1/4" alt="" />
            <div className='space-y-5'>
                <h2 className='font-medium'>{movie.tenPhim}</h2>
                <p>{movie.moTa}</p>
                <Progress percent={movie.danhGia * 10} />
            </div>
            <NavLink to={`/booking/${id}`}>
                <button className='rounded bg-red-300'>Mua ve</button>
            </NavLink>

        </div>
    )
}
