import React, { useEffect, useState } from 'react';
import { movieService } from '../services/movieService';
import ItemMovie from '../Components/ItemMovie';

export default function ListMovies() {

    let [movies, setMovies] = useState([]);

    useEffect(() => {
        movieService.getMovieList()
            .then((res) => {
                setMovies(res.data.content);
            }).catch((err) => {
                console.log(err);
            });
    }, [])


    return (
        <div className='container grid grid-cols-5 gap-10'>
            {movies.map((item) => {
                return <ItemMovie data={item} key={item.maPhim}></ItemMovie>
            })}
        </div>
    )
}
