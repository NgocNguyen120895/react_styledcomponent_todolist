import React from 'react'
import Header from '../Components/Header/Header'
import ListMovies from './ListMovies'
import TabMovie from './TabMovie/TabMovie'

export default function HomePage() {
    return (
        <div className='space-y-10'>
            <Header />
            <ListMovies />
            <TabMovie />
        </div>
    )
}
