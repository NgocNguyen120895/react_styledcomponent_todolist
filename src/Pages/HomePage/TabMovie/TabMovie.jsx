import React from 'react';
import { useEffect, useState } from 'react';
import { movieService } from '../../services/movieService';
import { Tabs } from 'antd';
// import ItemMovie from '../../Components/ItemMovie';
import ItemTabMovie from './ItemTabMovie';

export default function TabMovie() {

    let [heThongRap, setHeThongRap] = useState([])

    useEffect(() => {
        movieService.getMovieByTheater()
            .then((res) => {
                setHeThongRap(res.data.content);
                console.log(res.data.content)
            })
            .catch((err) => {
                console.log(err);
            });
    }, [])

    let renderHeThongRap = () => {
        return heThongRap.map(rap => {
            return (
                {
                    key: rap.maHeThongRap,
                    label: <img src={rap.logo} className='h-16' alt="" />,
                    children: (<Tabs style={{ height: 500 }} tabPosition="left" defaultActiveKey="1"
                        items={
                            rap.lstCumRap.map((cumRap) => {
                                return {
                                    key: cumRap.tenCumRap,
                                    label: <div>{cumRap.tenCumRap}</div>,
                                    children: (
                                        <div style={{ height: 500, width: "100%" }}
                                            className="grid grid-cols-1 overflow-y-scroll gap-5" >
                                            {
                                                cumRap.danhSachPhim.map((phim, index) => {
                                                    return (
                                                        <ItemTabMovie phim={phim} key={index} />
                                                    )
                                                })
                                            }
                                        </div >
                                    ),
                                }
                            })
                        } onChange={onChange} />),
                }
            )
        })
    }

    // const items = [
    //     {
    //         key: '1',
    //         label: `Tab 1`,
    //         children: `Content of Tab Pane 1`,
    //     },
    // ];

    const onChange = (key) => {
        console.log(key);
    };


    return (
        <div className='container'>
            <Tabs style={{ height: 600 }} tabPosition="left" defaultActiveKey="1" items={renderHeThongRap()} onChange={onChange} />
        </div>
    )
}
