import moment from 'moment/moment'
import React from 'react'

export default function ItemTabMovie({ phim }) {
    return (
        <div className='p-5 flex space-x-10'>

            <img src={phim.hinhAnh} className="w-24 h-32" alt="" />
            <div>
                <h3 className='font-medium text-xl'>{phim.tenPhim}</h3>
                <div className='grid grid-cols-3 gap-5 rounded'>
                    {phim.lstLichChieuTheoPhim.slice(0, 9).map((item, index) => {
                        return (
                            <span key={index} className='rounded p-2 text-white bg-red-500'>{moment(item.ngayChieuGioChieu).format("DD/MM/YYYY - hh:mm")}</span>
                        )
                    })}
                </div>
            </div>
        </div>
    )
}
