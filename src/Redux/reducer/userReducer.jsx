import { USER_LOGIN } from "../const/userConst";
import { localUserService } from "../../Pages/services/localService";


const initialState = {
    userInfo: localUserService.get() | null,
}

let userReducer = (state = initialState, { type, payload }) => {
    switch (type) {
        case USER_LOGIN: {
            return {
                ...state,
                userInfo: payload,
            }
        }

        default: {
            return state
        }
    }
}

export default userReducer