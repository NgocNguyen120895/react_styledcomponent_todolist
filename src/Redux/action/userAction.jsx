import { message } from "antd";
import { userSevices } from "../../Pages/services/userServices";
import { USER_LOGIN } from "../const/userConst";
import { localUserService } from "../../Pages/services/localService";



export const setLoginAction = (payload) => {
    return {
        type: USER_LOGIN,
        payload,
    }
}

export const setLoginActionService = (userValueForm, onCompleted) => {
    return (dispatch) => {
        userSevices.postLogin(userValueForm)
            .then((res) => {
                let action = {
                    type: USER_LOGIN,
                    payload: res.data.content,
                };
                dispatch(action);
                localUserService.set(res.data.content);
                onCompleted();
            })
            .catch((err) => {
                console.log(err);
            });
    }
}


// *payload -> axios respond (res.data.content)
// *userValueForm -> value from form